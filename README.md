# Team monitoring deployment conf

## Aims

Brings to life the team monitoring project
https://gitlab.com/teammonitoring/TeamMonitoringFront
https://gitlab.com/teammonitoring/TeamMonitoringBack

## Usage

```bash
docker-compose pull
docker-compose up -d
docker-compose down
```
